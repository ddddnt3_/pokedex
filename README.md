# PokéDex

PokéDex is a web application that allows users to search for Pokémon by name or number. The app also allows users to view the details of a Pokémon.

## Gitlab Page

Gitlab is a web application that allows users to view the details of a pokemon.

Show Project from: [https://fernando-jaramillo.gitlab.io/pokedex/](https://fernando-jaramillo.gitlab.io/pokedex/)

## About

Use: HTML, CSS, JS, Gitlab Pages

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)